Copyright 2019 - The LineageOS Project

Device configuration for Vsmart Active 1 Plus - PQ6002 (zero6pro_vin)
=====================================

Basic   | Spec Sheet
-------:|:-------------------------
CPU     | Octa-core (4x2.2 GHz Kryo & 4x1.8 GHz Kryo)
CHIPSET | Qualcomm SDM660 Snapdragon 660
GPU     | Adreno 512
Memory  | 6GB
Shipped Android Version | 8.1 (Oreo)
Storage | 64 GB
Battery | 3650 mAh
Dimensions | 156.1 x 76 x 7.95 mm
Display | 1080 x 2280 pixels, 6.18" LTPS IPS LCD
Rear Camera  | 12 MP main sensor / 24 MP second sensor
Front Camera | 20 MP

![Vsmart Active 1 Plus](https://cdn.tgdd.vn/Products/Images/42/196606/vsmart-active-1-plus-pink-18thangbh-400x460.png "Vsmart Active 1 Plus")
